package be.kdg.java2;

import be.kdg.java2.kollections.lists.List;
import be.kdg.java2.kollections.maps.ListMap;
import be.kdg.java2.kollections.maps.Map;
import be.kdg.java2.kollections.sets.ArraySet;
import be.kdg.java2.kollections.sets.Set;
import be.kdg.java2.kollections.sets.TreeSet;
import be.kdg.java2.kollections.trees.BinarySearchTree;
import be.kdg.java2.kollections.trees.Tree;

import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        testSetImplementation();
        //testMapImplementation();
        //testTreeImplementation();
    }

    private static void testSetImplementation() {
        Set<Student> studentSet = new ArraySet<>();
        for (int i = 0; i < 1000; i++) {
            studentSet.add(new Student(new Random().nextInt(100)));
        }
        List<Student> studentList = studentSet.toList();
        for (int i = 0; i < studentList.size(); i++) {
            System.out.println(studentList.get(i));
        }
        System.out.println(studentSet.size());
    }

    private static void testMapImplementation() {
        Map<Student, String> studentMap;
        studentMap = new ListMap<>();
        //studentMap = new HashMap<>();

        for (int i = 0; i < 100; i++) {
            studentMap.put(new Student(i, "john" + i), "teststring" + i);
        }
        System.out.println("Map created with " + studentMap.size() + " elements");
    }

    private static void testTreeImplementation() {
        Tree<Student> tree = new Tree<>();
        tree.setRoot(new Student(1,"Jos"));
        Tree.TreeNode<Student> root = tree.getRoot();
        Tree.TreeNode<Student> jefNode = tree.add(root, new Student(2,"Jef"));
        Tree.TreeNode<Student> dirkNode = tree.add(root, new Student(3,"Dirk"));
        Tree.TreeNode<Student> fonsNode = tree.add(root, new Student(4,"Fons"));
        Tree.TreeNode<Student> fransNode = tree.add(jefNode,new Student(5, "Frans"));
        Tree.TreeNode<Student> miaNode = tree.add(jefNode, new Student(6,"Mia"));
        Tree.TreeNode<Student> leenNode = tree.add(fonsNode,new Student(7, "Leen"));
        Tree.TreeNode<Student> anNode = tree.add(leenNode, new Student(8,"An"));
        Tree.TreeNode<Student> sonjaNode = tree.add(leenNode, new Student(9,"Sonja"));
        System.out.println("Depth first:");
        tree.traverseDepthFirst(System.out::println);
        System.out.println("Breadth first:");
        tree.traverseBreadthFirst(System.out::println);
        XMLGenerator xmlGenerator = new XMLGenerator(tree);
        xmlGenerator.generateXML();
        System.out.println("BST:");
        BinarySearchTree<Student> bst = new BinarySearchTree<>();
        tree.traverseDepthFirst(bst::add);
        bst.traverseInOrder(System.out::println);
    }
}
